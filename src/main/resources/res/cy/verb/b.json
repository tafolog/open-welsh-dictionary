[
  {
    "normalForm": "bod",
    "id": 0,
    "translations": [ "to be" ],
    "stem": "bydd",
    "notes": "The conjugation of the verb bod is very irregular. The conjugation tables below are not exhaustive, but represents some of the more common variants and tenses. Where bod is used as an auxiliary verb it is always linked with yn, wedi, or some other particle.",
    "present_affirmative": {
      "singFirst": ["rwyf", "rydw", "dw" ],
      "singSecond": "rwyt",
      "singThird": ["mae", "ydy", "yw", "sy"],
      "plurFirst": ["rydyn", "dyn","dan" ],
      "plurSecond": ["rydych", "dych", "dach" ],
      "plurThird": ["maent", "maen", "ydynt" ]
    },
    "present_negative": {
      "singFirst": ["dwyf","dydw","dw" ],
      "singSecond": "dwyt",
      "singThird": ["dydy", "dyw", "does", "sy"],
      "plurFirst": ["dydyn", "dyn","dan" ],
      "plurSecond": ["dydych", "dych", "dach"],
      "plurThird": ["dydyn" ]
    },
    "present_interrogative": {
      "singFirst": "ydw",
      "singSecond": "wyt",
      "singThird": [ "ydy", "yw", "oes", "sy"],
      "plurFirst": [ "ydyn", "ydan"],
      "plurSecond": ["ydych", "dych", "dach"],
      "plurThird": [ "ydyn" ]
    },
    "imperfect_affirmative": {
      "stem": "roedd",
      "singThird": "roedd"
    },
    "imperfect_negative": {
      "stem": "doedd",
      "singThird": "doedd"
    },
    "imperfect_interrogative": {
      "stem": "oedd",
      "singThird": "oedd"
    },
    "preterite": {
      "stem": "bu",
      "singFirst": "bues",
      "singSecond": "buest"
    },
    "future": {
      "singThird": "bydd"
    },
    "examples": [
      {
        "text": "<strong>Rydw</strong> i'n mynd",
        "trans": "I am going",
        "explanation": "The word to be (in this case rydw) is used to form present tense."
      },
      {
        "text": "Mae hi'n meddwl (fy) <strong>mod</strong> i'n cŵl",
        "trans": "She thinks that I am cool",
        "explanation": "Bod is also used to serve the same purpose as the English 'that'. Note that bod mutates based on the pronoun, though the pronoun in front is sometimes dropped in speech."
      }
    ],
    "ipa": [
      {
        "key": "",
        "value": "/boːd/"
      }
    ]
  },
  {
    "normalForm": "baglu",
    "id": 23,
    "translations": [ "to stumble" ]
  },
  {
    "normalForm": "bancio",
    "id": 102
  },
  {
    "normalForm": "barnu",
    "id": 103
  },
  {
    "normalForm": "beicio",
    "id": 104
  },
  {
    "normalForm": "beichiogi",
    "id": 105
  },
  {
    "normalForm": "beiddio",
    "id": 106
  },
  {
    "normalForm": "beirniadu",
    "id": 107
  },
  {
    "normalForm": "bendigo",
    "id": 108
  },
  {
    "normalForm": "bendithio",
    "id": 109
  },
  {
    "normalForm": "benthyg",
    "id": 110
  },
  {
    "normalForm": "berwi",
    "id": 111
  },
  {
    "normalForm": "blingo",
    "id": 112
  },
  {
    "normalForm": "blino",
    "id": 113
  },
  {
    "normalForm": "bocsio",
    "id": 114
  },
  {
    "normalForm": "boddi",
    "id": 115
  },
  {
    "normalForm": "braenu",
    "id": 116
  },
  {
    "normalForm": "brathu",
    "id": 117
  },
  {
    "normalForm": "brawychu",
    "id": 118
  },
  {
    "normalForm": "breuddwydio",
    "id": 119
  },
  {
    "normalForm": "briwsioni",
    "id": 120
  },
  {
    "normalForm": "brwydro",
    "id": 121
  },
  {
    "normalForm": "brwyna",
    "id": 122
  },
  {
    "normalForm": "bwrw",
    "stem": "bwri",
    "id": 123
  },
  {
    "normalForm": "bwydo",
    "id": 124
  },
  {
    "normalForm": "bwyta",
    "id": 125,
    "translations": [ "to eat" ]
  },
  {
    "normalForm": "byddaru",
    "id": 126
  },
  {
    "normalForm": "bygwth",
    "id": 127
  },
  {
    "normalForm": "byw",
    "id": 128,
    "notes": "Byw does not conjugate and needs to be used with periphrasis.",
    "conjugates": false
  },
  {
    "normalForm": "blocio",
    "id": 798,
    "translations": [ "to block" ]
  },
  {
    "normalForm": "basgedu",
    "id": 802,
    "translations": [ "to put in a basket" ]
  },
  {
    "normalForm": "bildio",
    "id": 816,
    "translations": [ "to build" ]
  },
  {
    "normalForm": "byrlymu",
    "id": 829,
    "translations": [ "to bubble" ]
  },
  {
    "normalForm": "blotio",
    "id": 835
  },
  {
    "normalForm": "bygylu",
    "id": 874,
    "translations": [ "to terrify" ]
  },
  {
    "normalForm": "bancawio",
    "id": 884,
    "translations": [ "to bind" ]
  },
  {
    "normalForm": "bysu",
    "id": 888,
    "translations": [ "to finger" ]
  },
  {
    "normalForm": "bucheddu",
    "id": 931,
    "translations": [ "to reside" ]
  },
  {
    "normalForm": "botymu",
    "id": 937,
    "translations": [ "to button" ]
  },
  {
    "normalForm": "bloeddio",
    "id": 940,
    "translations": [ "to shout" ]
  },
  {
    "normalForm": "brasgamu",
    "stem": "brasgem",
    "id": 945,
    "translations": [ "to stride" ]
  },
  {
    "normalForm": "brigdorri",
    "id": 951,
    "translations": [ "to lop" ]
  },
  {
    "normalForm": "bragio",
    "id": 1002,
    "translations": [ "to brag" ]
  },
  {
    "normalForm": "bihafio",
    "id": 1008,
    "translations": [ "to behave" ]
  },
  {
    "normalForm": "bownsio",
    "id": 1062,
    "translations": [ "to bounce" ]
  },
  {
    "normalForm": "batingo",
    "id": 1072,
    "translations": [ "to hoe" ]
  },
  {
    "normalForm": "brwydo",
    "id": 1084,
    "translations": [ "to fashion" ]
  },
  {
    "normalForm": "bongamu",
    "stem": "bongem",
    "id": 1086,
    "translations": [ "to waddle" ]
  },
  {
    "normalForm": "begera",
    "id": 1100,
    "translations": [ "to beg" ]
  },
  {
    "normalForm": "bagio",
    "id": 1110,
    "translations": [ "to bag" ]
  },
  {
    "normalForm": "baldorddi",
    "id": 1154,
    "translations": [ "to babble" ]
  },
  {
    "normalForm": "brecio",
    "id": 1165,
    "translations": [ "to brake" ]
  },
  {
    "normalForm": "buchfrechu",
    "id": 1198,
    "translations": [ "to vaccinate" ]
  },
  {
    "normalForm": "bopio",
    "id": 1231,
    "translations": [ "to bop" ]
  },
  {
    "normalForm": "brechu",
    "id": 1240,
    "translations": [ "to vaccinate" ]
  },
  {
    "normalForm": "byrhau",
    "stem": "byrhe",
    "id": 1249,
    "translations": [ "to shorten" ]
  },
  {
    "normalForm": "blaenllymu",
    "id": 1250,
    "translations": [ "to sharpen" ]
  },
  {
    "normalForm": "blaengynllunio",
    "id": 1255,
    "translations": [ "to blueprint" ]
  },
  {
    "normalForm": "brydio",
    "id": 1329,
    "translations": [ "to intend" ]
  },
  {
    "normalForm": "breisgio",
    "id": 1332,
    "translations": [ "to become fat" ]
  },
  {
    "normalForm": "benthyca",
    "id": 1351,
    "translations": [ "to lend" ]
  },
  {
    "normalForm": "balansio",
    "id": 1385,
    "translations": [ "to balance" ]
  },
  {
    "normalForm": "brigladd",
    "stem": "brigledd",
    "id": 1390,
    "translations": [ "to lop the top off" ]
  },
  {
    "normalForm": "banio",
    "id": 1391,
    "translations": [ "to ban" ]
  },
  {
    "normalForm": "byclu",
    "id": 1392,
    "translations": [ "to buckle" ]
  },
  {
    "normalForm": "byclo",
    "id": 1398,
    "translations": [ "to buckle" ]
  },
  {
    "normalForm": "blero",
    "id": 1449,
    "translations": [ "to blare" ]
  },
  {
    "normalForm": "benyweta",
    "id": 1482,
    "translations": [ "to whore" ]
  },
  {
    "normalForm": "bwlian",
    "id": 1502,
    "translations": [ "to bully" ]
  },
  {
    "normalForm": "byrstio",
    "id": 1511,
    "translations": [ "to burst" ]
  },
  {
    "normalForm": "breuhau",
    "stem": "breuhe",
    "id": 1521,
    "translations": [ "to become fragile" ]
  },
  {
    "normalForm": "brafio",
    "id": 1523,
    "translations": [ "to brighten" ]
  },
  {
    "normalForm": "blaenddodi",
    "id": 1528,
    "translations": [ "to prefix" ]
  },
  {
    "normalForm": "beisio",
    "id": 1586,
    "translations": [ "to ford" ]
  },
  {
    "normalForm": "busnesu",
    "id": 1602,
    "translations": [ "to meddle" ]
  },
  {
    "normalForm": "busnesa",
    "id": 1606,
    "translations": [ "to meddle" ]
  },
  {
    "normalForm": "briwio",
    "id": 1609,
    "translations": [ "to mince" ]
  },
  {
    "normalForm": "bawdfedi",
    "id": 1655,
    "translations": [ "to reap by hand" ]
  },
  {
    "normalForm": "bombardio",
    "id": 1664,
    "translations": [ "to bombard" ]
  },
  {
    "normalForm": "bradlofruddio",
    "id": 1711,
    "translations": [ "to assassinate" ]
  },
  {
    "normalForm": "buarthu",
    "id": 1721,
    "translations": [ "to pen" ]
  },
  {
    "normalForm": "budrelwa",
    "stem": "budrelwa",
    "id": 1769,
    "translations": [ "to profiteer" ]
  },
  {
    "normalForm": "barugo",
    "id": 1781,
    "translations": [ "to get frosty" ]
  },
  {
    "normalForm": "brasnaddu",
    "id": 1801,
    "translations": [ "to rough-hew" ]
  },
  {
    "normalForm": "bwldagu",
    "id": 1817,
    "translations": [ "to belch" ]
  },
  {
    "normalForm": "bolaheulo",
    "id": 1820,
    "translations": [ "to sunbathe" ]
  },
  {
    "normalForm": "brychu",
    "id": 1835,
    "translations": [ "to fleck" ]
  },
  {
    "normalForm": "bostio",
    "id": 1864,
    "translations": [ "to boast" ]
  },
  {
    "normalForm": "barbio",
    "id": 1885,
    "translations": [ "to clip hedge" ]
  },
  {
    "normalForm": "biwsio",
    "id": 1903,
    "translations": [ "to abuse" ]
  },
  {
    "normalForm": "bonllefain",
    "id": 1921,
    "translations": [ "to roar" ]
  },
  {
    "normalForm": "breintio",
    "id": 1966,
    "translations": [ "to honour" ]
  },
  {
    "normalForm": "bwndelu",
    "id": 1969,
    "translations": [ "to bundle" ]
  },
  {
    "normalForm": "bradychu",
    "id": 1970,
    "translations": [ "to betray" ]
  },
  {
    "normalForm": "blysu",
    "id": 1977,
    "translations": [ "to crave" ]
  },
  {
    "normalForm": "bangio",
    "id": 1987,
    "translations": [ "to bang" ]
  },
  {
    "normalForm": "buddio",
    "id": 1993,
    "translations": [ "to profit" ]
  },
  {
    "normalForm": "brysgerdded",
    "id": 2026,
    "translations": [ "to walk briskly" ]
  },
  {
    "normalForm": "bryweddu",
    "id": 2049,
    "translations": [ "to brew" ]
  },
  {
    "normalForm": "bowndio",
    "id": 2087,
    "translations": [ "to bound" ]
  },
  {
    "normalForm": "brawddegu",
    "id": 2121,
    "translations": [ "to phrase" ]
  },
  {
    "normalForm": "byrnio",
    "id": 2170,
    "translations": [ "to bundle" ]
  },
  {
    "normalForm": "bugeilio",
    "id": 2216,
    "translations": [ "to shepherd" ]
  },
  {
    "normalForm": "briwlan",
    "stem": "briwlan",
    "id": 2260,
    "translations": [ "to drizzle" ]
  },
  {
    "normalForm": "breibio",
    "id": 2264,
    "translations": [ "to bribe" ]
  },
  {
    "normalForm": "blaendalu",
    "stem": "blaendel",
    "id": 2266,
    "translations": [ "to pay in advance" ]
  },
  {
    "normalForm": "blaguro",
    "id": 2326,
    "translations": [ "to sprout" ]
  },
  {
    "normalForm": "bodoli",
    "id": 2342,
    "translations": [ "to exist" ]
  },
  {
    "normalForm": "bustachu",
    "id": 2348,
    "translations": [ "to blunder about" ]
  },
  {
    "normalForm": "betio",
    "id": 2380,
    "translations": [ "to bet" ]
  },
  {
    "normalForm": "bytheirio",
    "id": 2384,
    "translations": [ "to belch" ]
  },
  {
    "normalForm": "barcuta",
    "id": 2387,
    "translations": [ "to hang-glide" ]
  },
  {
    "normalForm": "briwlio",
    "id": 2396,
    "translations": [ "to broil" ]
  },
  {
    "normalForm": "blaenoriaethu",
    "id": 2400,
    "translations": [ "to take precedence" ]
  },
  {
    "normalForm": "bonclustio",
    "id": 2411,
    "translations": [ "to box on the ears" ]
  },
  {
    "normalForm": "brewlan",
    "stem": "brewlan",
    "id": 2414,
    "translations": [ "to drizzle" ]
  },
  {
    "normalForm": "brifo",
    "id": 2424,
    "translations": [ "to hurt" ]
  },
  {
    "normalForm": "brigo",
    "id": 2442,
    "translations": [ "to sprout" ]
  },
  {
    "normalForm": "bricio",
    "id": 2473,
    "translations": [ "to brick" ]
  },
  {
    "normalForm": "bolheulo",
    "id": 2524,
    "translations": [ "to sunbathe" ]
  },
  {
    "normalForm": "bridio",
    "id": 2568,
    "translations": [ "to breed" ]
  },
  {
    "normalForm": "brasbwythu",
    "id": 2573,
    "translations": [ "to sew with large stitches" ]
  },
  {
    "normalForm": "brasbwytho",
    "id": 2574,
    "translations": [ "to baste" ]
  },
  {
    "normalForm": "bablan",
    "id": 2578,
    "translations": [ "to babble" ]
  },
  {
    "normalForm": "blipio",
    "id": 2587,
    "translations": [ "to bleep" ]
  },
  {
    "normalForm": "bigitan",
    "id": 2607,
    "translations": [ "to provoke" ]
  },
  {
    "normalForm": "blaenwerthu",
    "id": 2617,
    "translations": [ "to pre-sell" ]
  },
  {
    "normalForm": "baeddu",
    "id": 2631,
    "translations": [ "to soil" ]
  },
  {
    "normalForm": "bragu",
    "id": 2658,
    "translations": [ "to brew" ]
  },
  {
    "normalForm": "briwo",
    "stem": "bri",
    "id": 2662,
    "translations": [ "to crush" ]
  },
  {
    "normalForm": "becso",
    "id": 2675,
    "translations": [ "to worry" ]
  },
  {
    "normalForm": "bwriaf",
    "id": 2707
  },
  {
    "normalForm": "bylchu",
    "id": 2708,
    "translations": [ "to breach" ]
  },
  {
    "normalForm": "bathio",
    "id": 2710,
    "translations": [ "to bathe" ]
  },
  {
    "normalForm": "beichio",
    "id": 2720,
    "translations": [ "to sob" ]
  },
  {
    "normalForm": "brysio",
    "id": 2722,
    "translations": [ "to hurry" ]
  },
  {
    "normalForm": "bradu",
    "id": 2758,
    "translations": [ "to betray" ]
  },
  {
    "normalForm": "brwysgio",
    "id": 2763,
    "translations": [ "to stir" ]
  },
  {
    "normalForm": "brecwasta",
    "id": 2768,
    "translations": [ "to breakfast" ]
  },
  {
    "normalForm": "bastio",
    "id": 2845,
    "translations": [ "to baste" ]
  },
  {
    "normalForm": "blogio",
    "id": 2881,
    "translations": [ "to blog" ]
  },
  {
    "normalForm": "bugunad",
    "id": 2910,
    "translations": [ "to bellow" ]
  },
  {
    "normalForm": "bathodi",
    "id": 2912,
    "translations": [ "to coin" ]
  },
  {
    "normalForm": "brolio",
    "id": 2935,
    "translations": [ "to boast" ]
  },
  {
    "normalForm": "budro",
    "id": 2960,
    "translations": [ "to soil" ]
  },
  {
    "normalForm": "baetio",
    "id": 2968,
    "translations": [ "to bait" ]
  },
  {
    "normalForm": "baneru",
    "id": 2988,
    "translations": [ "to flag up" ]
  },
  {
    "normalForm": "batio",
    "id": 3002,
    "translations": [ "to bat" ]
  },
  {
    "normalForm": "bathu",
    "id": 3010,
    "translations": [ "to coin" ]
  },
  {
    "normalForm": "britho",
    "id": 3011,
    "translations": [ "to dapple" ]
  },
  {
    "normalForm": "brigbori",
    "id": 3014,
    "translations": [ "to browse" ]
  },
  {
    "normalForm": "bradfwriadu",
    "id": 3023,
    "translations": [ "to conspire" ]
  },
  {
    "normalForm": "boddhau",
    "stem": "boddhe",
    "id": 3069,
    "translations": [ "to please" ]
  },
  {
    "normalForm": "bywiogi",
    "id": 3070,
    "translations": [ "to animate" ]
  },
  {
    "normalForm": "bywlunio",
    "id": 3072,
    "translations": [ "to animate" ]
  },
  {
    "normalForm": "bagadu",
    "id": 3130,
    "translations": [ "to cluster" ]
  },
  {
    "normalForm": "berweddu",
    "id": 3138,
    "translations": [ "to brew" ]
  },
  {
    "normalForm": "brodio",
    "id": 3175,
    "translations": [ "to embroider" ]
  },
  {
    "normalForm": "bracso",
    "id": 3193,
    "translations": [ "to pull through" ]
  },
  {
    "normalForm": "bodloni",
    "id": 3204,
    "translations": [ "to satisfy" ]
  },
  {
    "normalForm": "bobio",
    "id": 3210,
    "translations": [ "to bob" ]
  },
  {
    "normalForm": "brithweithio",
    "id": 3246,
    "translations": [ "to tessellate" ]
  },
  {
    "normalForm": "blasu",
    "id": 3257,
    "translations": [ "to taste" ]
  },
  {
    "normalForm": "beio",
    "id": 3271,
    "translations": [ "to blame" ]
  },
  {
    "normalForm": "brasio",
    "id": 3272,
    "translations": [ "to do a rough job" ]
  },
  {
    "normalForm": "brwsio",
    "id": 3318,
    "translations": [ "to brush" ]
  },
  {
    "normalForm": "bosio",
    "id": 3324,
    "translations": [ "to boss" ]
  },
  {
    "normalForm": "bowlio",
    "id": 3334,
    "translations": [ "to bowl" ]
  },
  {
    "normalForm": "boddro",
    "id": 3338,
    "translations": [ "to bother" ]
  },
  {
    "normalForm": "bedyddio",
    "id": 3358,
    "translations": [ "to baptise" ]
  },
  {
    "normalForm": "blonegu",
    "id": 3387,
    "translations": [ "to grease" ]
  },
  {
    "normalForm": "boglynnu",
    "id": 3389,
    "translations": [ "to emboss" ]
  },
  {
    "normalForm": "bwrlymu",
    "id": 3407,
    "translations": [ "to gurgle" ]
  },
  {
    "normalForm": "boddloni",
    "id": 3439,
    "translations": [ "to gratify" ]
  },
  {
    "normalForm": "bario",
    "id": 3447,
    "translations": [ "to bar" ]
  },
  {
    "normalForm": "brudio",
    "id": 3455,
    "translations": [ "to prophesy" ]
  },
  {
    "normalForm": "bennu",
    "id": 3458,
    "translations": [ "to finish" ]
  },
  {
    "normalForm": "bargodi",
    "id": 3473,
    "translations": [ "to project" ]
  },
  {
    "normalForm": "bargeinio",
    "id": 3484,
    "translations": [ "to bargain" ]
  },
  {
    "normalForm": "byrddio",
    "id": 3485,
    "translations": [ "to board" ]
  },
  {
    "normalForm": "blaenori",
    "id": 3493,
    "translations": [ "to precede" ]
  },
  {
    "normalForm": "bloesgi",
    "id": 3501,
    "translations": [ "to speak indistinctly" ]
  },
  {
    "normalForm": "buddioli",
    "id": 3524,
    "translations": [ "to profit" ]
  },
  {
    "normalForm": "basaf",
    "id": 3546,
    "translations": [ "to sing bass" ]
  },
  {
    "normalForm": "brochi",
    "id": 3641,
    "translations": [ "to foam" ]
  },
  {
    "normalForm": "begian",
    "id": 3654,
    "translations": [ "to beg" ]
  },
  {
    "normalForm": "blaendirio",
    "id": 3662,
    "translations": [ "to foreground" ]
  },
  {
    "normalForm": "babanu",
    "id": 3664,
    "translations": [ "to spoil" ]
  },
  {
    "normalForm": "bysellu",
    "id": 3726,
    "translations": [ "to key in" ]
  },
  {
    "normalForm": "blaenyrru",
    "id": 3729,
    "translations": [ "to forward" ]
  },
  {
    "normalForm": "blysio",
    "id": 3740,
    "translations": [ "to crave" ]
  },
  {
    "normalForm": "bytholi",
    "id": 3741,
    "translations": [ "to perpetuate" ]
  },
  {
    "normalForm": "bysio",
    "id": 3784,
    "translations": [ "to finger" ]
  },
  {
    "normalForm": "breinio",
    "id": 3791,
    "translations": [ "to honour" ]
  },
  {
    "normalForm": "blaendorri",
    "id": 3811,
    "translations": [ "to truncate" ]
  },
  {
    "normalForm": "bordio",
    "id": 3835,
    "translations": [ "to board" ]
  },
  {
    "normalForm": "byddino",
    "id": 3845,
    "translations": [ "to marshal" ]
  },
  {
    "normalForm": "bilio",
    "id": 3879,
    "translations": [ "to bill" ]
  },
  {
    "normalForm": "bregysu",
    "id": 3944,
    "translations": [ "to ferment" ]
  },
  {
    "normalForm": "bygythio",
    "id": 3945,
    "translations": [ "to threaten" ]
  },
  {
    "normalForm": "baricedio",
    "id": 3957,
    "translations": [ "to barricade" ]
  },
  {
    "normalForm": "breichio",
    "id": 3966,
    "translations": [ "to link arms" ]
  },
  {
    "normalForm": "bwriadu",
    "stem": "bwried",
    "id": 3978,
    "translations": [ "to intend" ]
  },
  {
    "normalForm": "braslunio",
    "id": 4040,
    "translations": [ "to sketch" ]
  },
  {
    "normalForm": "benthycio",
    "id": 4043,
    "translations": [ "to borrow" ]
  },
  {
    "normalForm": "breferu",
    "id": 4048,
    "translations": [ "to bellow" ]
  },
  {
    "normalForm": "brandio",
    "id": 4058,
    "translations": [ "to brand" ]
  },
  {
    "normalForm": "byseddu",
    "id": 4124,
    "translations": [ "to finger" ]
  },
  {
    "normalForm": "bidio",
    "id": 4137,
    "translations": [ "to bid" ]
  },
  {
    "normalForm": "blaenbrofi",
    "id": 4146,
    "translations": [ "to anticipate" ]
  },
  {
    "normalForm": "bwhwman",
    "id": 4147,
    "translations": [ "to vacillate" ]
  },
  {
    "normalForm": "braenaru",
    "stem": "braener",
    "id": 4159,
    "translations": [ "to fallow" ]
  },
  {
    "normalForm": "barbareiddio",
    "id": 4179,
    "translations": [ "to barbarise" ]
  },
  {
    "normalForm": "blasuso",
    "id": 4192,
    "translations": [ "to flavour" ]
  },
  {
    "normalForm": "blynyddoli",
    "id": 4195,
    "translations": [ "to annualise" ]
  },
  {
    "normalForm": "blacmelio",
    "id": 4200,
    "translations": [ "to blackmail" ]
  },
  {
    "normalForm": "brefu",
    "id": 4228,
    "translations": [ "to bleat" ]
  },
  {
    "normalForm": "blodio",
    "id": 4236,
    "translations": [ "to make meal" ]
  },
  {
    "normalForm": "bodio",
    "id": 4245,
    "translations": [ "to thumb" ]
  },
  {
    "normalForm": "becsio",
    "id": 4260,
    "translations": [ "to worry" ]
  },
  {
    "normalForm": "bragaldio",
    "id": 4272,
    "translations": [ "to jabber" ]
  },
  {
    "normalForm": "buddsoddi",
    "id": 4280,
    "translations": [ "to invest" ]
  },
  {
    "normalForm": "blastio",
    "id": 4284,
    "translations": [ "to blast" ]
  },
  {
    "normalForm": "bacsu",
    "id": 4296,
    "translations": [ "to muddle through" ]
  },
  {
    "normalForm": "bychanu",
    "id": 4334,
    "translations": [ "to belittle" ]
  },
  {
    "normalForm": "begio",
    "id": 4344,
    "translations": [ "to beg" ]
  },
  {
    "normalForm": "benthyg",
    "id": 4357,
    "translations": [ "to lend" ]
  },
  {
    "normalForm": "barneisio",
    "id": 4363,
    "translations": [ "to varnish" ]
  },
  {
    "normalForm": "bolltio",
    "id": 4364,
    "translations": [ "to bolt" ]
  },
  {
    "normalForm": "bachu",
    "id": 4378,
    "translations": [ "to hook" ]
  },
  {
    "normalForm": "bywhau",
    "stem": "bywhe",
    "id": 4383,
    "translations": [ "to animate" ]
  },
  {
    "normalForm": "biledu",
    "id": 4399,
    "translations": [ "to billet" ]
  },
  {
    "normalForm": "bacio",
    "id": 4406,
    "translations": [ "to back" ]
  },
  {
    "normalForm": "bwcio",
    "id": 4415,
    "translations": [ "to book" ]
  },
  {
    "normalForm": "brwyno",
    "id": 4416,
    "translations": [ "to grieve" ]
  },
  {
    "normalForm": "brasamcanu",
    "id": 4423,
    "translations": [ "to approximate" ]
  },
  {
    "normalForm": "barddoni",
    "id": 4452,
    "translations": [ "to write poetry" ]
  },
  {
    "normalForm": "bidogi",
    "id": 4470,
    "translations": [ "to bayonet" ]
  },
  {
    "normalForm": "breicheidio",
    "id": 4493,
    "translations": [ "to embrace" ]
  },
  {
    "normalForm": "breuo",
    "id": 4549,
    "translations": [ "to become brittle" ]
  },
  {
    "normalForm": "brwylio",
    "id": 4581,
    "translations": [ "to broil" ]
  },
  {
    "normalForm": "blodeuo",
    "id": 4598,
    "translations": [ "to flower" ]
  },
  {
    "normalForm": "bomio",
    "id": 4601,
    "translations": [ "to bomb" ]
  },
  {
    "normalForm": "bwlio",
    "id": 4604,
    "translations": [ "to bully" ]
  },
  {
    "normalForm": "bywiocâf",
    "id": 4610,
    "translations": [ "to live" ]
  },
  {
    "normalForm": "bochio",
    "id": 4618,
    "translations": [ "to gobble" ]
  },
  {
    "normalForm": "byrnu",
    "id": 4619,
    "translations": [ "to bundle" ]
  },
  {
    "normalForm": "blaenbeipio",
    "id": 4621,
    "translations": [ "to pipeline" ]
  },
  {
    "normalForm": "blaendarddu",
    "id": 4658,
    "translations": [ "to sprout" ]
  },
  {
    "normalForm": "bwnglera",
    "id": 4668,
    "translations": [ "to bungle" ]
  }
]
  }
]