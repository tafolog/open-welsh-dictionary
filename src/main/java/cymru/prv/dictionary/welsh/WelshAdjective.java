package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents a Welsh adjective
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class WelshAdjective extends WelshWord {

    private static final String EQUATIVE = "equative";
    private static final String COMPARATIVE = "comparative";
    private static final String SUPERLATIVE = "superlative";

    private static final int syllableThreshold = 2;

    private final long syllables;
    private final String stem;

    private final boolean comparable;

    private final List<String> equative;
    private final List<String> comparative;
    private final List<String> superlative;

    public WelshAdjective(Dictionary dictionary, JSONObject obj){
        super(dictionary, obj, WordType.adjective);
        syllables = Syllable.countSyllables(getNormalForm());

        stem = obj.optString("stem", createStem(getNormalForm()));

        comparable = obj.optBoolean("comparable", true);
        if(comparable) {
            equative = obj.has(EQUATIVE) ? Json.getStringList(obj, EQUATIVE) : getEquative();
            comparative = obj.has(COMPARATIVE) ? Json.getStringList(obj, COMPARATIVE) : getComparative();
            superlative = obj.has(SUPERLATIVE) ? Json.getStringList(obj, SUPERLATIVE) : getSuperlative();
        }
        else {
            equative = comparative = superlative = null;
        }
    }

    protected List<String> getEquative(){
        if(syllables > syllableThreshold) {
            var soft = new WelshMutation(getNormalForm()).getSoft();
            return Collections.singletonList("mor " + (soft != null ? soft : getNormalForm()));
        }
        return Collections.singletonList(stem + "ed");
    }

    protected List<String> getComparative(){
        if(syllables > syllableThreshold)
            return Collections.singletonList("mwy " + getNormalForm());
        return Collections.singletonList(stem + "ach");
    }

    protected List<String> getSuperlative(){
        if(syllables > syllableThreshold)
            return Collections.singletonList("mwya " + getNormalForm());
        return Collections.singletonList(stem + "af");
    }


    private String createStem(String normalForm){
        if(syllables > 2)
            return null;
        if(normalForm.endsWith("b"))
            return normalForm.replaceFirst("b$", "p");
        if(normalForm.endsWith("g"))
            return normalForm.replaceFirst("g$", "c");
        return normalForm;
    }

    @Override
    protected JSONObject getInflections() {
        if(!comparable)
            return null;
        JSONObject obj = new JSONObject();
        obj.put(EQUATIVE, equative);
        obj.put(COMPARATIVE, comparative);
        obj.put(SUPERLATIVE, superlative);
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(comparable) {
            list.addAll(equative);
            list.addAll(comparative);
            list.addAll(superlative);
        }
        return list;
    }
}
