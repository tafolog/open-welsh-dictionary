package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;


/**
 * Represents a Welsh word
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class WelshWord extends Word {

    public WelshWord(Dictionary dictionary, JSONObject obj, WordType type) {
        super(dictionary, obj, type);
    }

    @Override
    protected JSONObject getMutations() {
        return new WelshMutation(getNormalForm()).toJson();
    }
}
