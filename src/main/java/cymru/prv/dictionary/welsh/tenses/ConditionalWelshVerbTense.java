package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * Represents the conditional tense in Welsh
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class ConditionalWelshVerbTense extends WelshVerbTense {

    public ConditionalWelshVerbTense(WelshVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        if(stem == null)
            System.out.println("");
        return Arrays.asList(apply("wn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply("et"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Arrays.asList(apply("ai"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Arrays.asList(apply("en"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("ech"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Arrays.asList(apply("en"));
    }
}
