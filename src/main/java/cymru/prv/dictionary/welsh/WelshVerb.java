package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.welsh.tenses.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;


/**
 * Represents a Welsh verb
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class WelshVerb extends WelshWord {

    private final String stem;
    private final Map<String, Conjugation> tenses = new HashMap<>();

    public WelshVerb(Dictionary dictionary, JSONObject obj){
        super(dictionary, obj, WordType.verb);
        stem = obj.optString("stem", getNormalForm().replaceFirst("(a|i|o|u|eg|ed|an|yd|yll)$", ""));

        // Required tenses. Will be generated if not present
        addRequiredTense(obj, "preterite", PreteriteWelshVerbTense::new);
        addRequiredTense(obj, "future", FutureWelshVerbTense::new);
        addRequiredTense(obj, "imperative", ImperativeWelshVerbTense::new);
        addRequiredTense(obj, "conditional", ConditionalWelshVerbTense::new);
        addRequiredTense(obj, "pluperfect", PluperfectTenseWelsh::new);

        // Optional tenses. Will not be generated
        addOptionalTense(obj, "present", this::getEmptyConjugation);
        addOptionalTense(obj, "present_affirmative", this::getEmptyConjugation);
        addOptionalTense(obj, "present_negative", this::getEmptyConjugation);
        addOptionalTense(obj, "present_interrogative", this::getEmptyConjugation);
        addOptionalTense(obj, "imperfect", ConditionalWelshVerbTense::new);
        addOptionalTense(obj, "imperfect_affirmative", ConditionalWelshVerbTense::new);
        addOptionalTense(obj, "imperfect_negative", ConditionalWelshVerbTense::new);
        addOptionalTense(obj, "imperfect_interrogative", ConditionalWelshVerbTense::new);
    }

    private Conjugation getEmptyConjugation(WelshVerb verb, JSONObject obj){
        return new Conjugation(obj);
    }

    private void addRequiredTense(JSONObject obj, String key, BiFunction<WelshVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
        else
            tenses.put(key, func.apply(this, new JSONObject()));
    }

    private void addOptionalTense(JSONObject obj, String key, BiFunction<WelshVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
    }

    public String getStem() {
        return stem;
    }

    @Override
    protected JSONObject getConjugations() {
        JSONObject obj =  new JSONObject();
        for(String key : tenses.keySet())
            obj.put(key, tenses.get(key).toJson());
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        for(Conjugation conjugation : tenses.values())
            list.addAll(conjugation.getVersions());
        return list;
    }
}
