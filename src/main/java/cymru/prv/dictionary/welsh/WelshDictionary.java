package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.BiFunction;


/**
 * Represents a Welsh dictionary.
 *
 * Creates a dictionary with the code "cy"
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Dictionary
 */
public class WelshDictionary extends Dictionary {

    private static final String LANG_CODE = "cy";

    private static final Map<WordType, BiFunction<Dictionary, JSONObject, Word>> types = Map.of(
            WordType.adjective, WelshAdjective::new,
            WordType.conjunction, (d,w) -> new Word(d, w, WordType.conjunction),
            WordType.noun, WelshNoun::new,
            WordType.adposition, WelshAdposition::new,
            WordType.verb, WelshVerb::new
    );

    public WelshDictionary() {
        super(LANG_CODE, types);
    }

    public WelshDictionary(DictionaryList list) {
        super(list, LANG_CODE, types);
    }
}
