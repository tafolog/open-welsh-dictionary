package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Word;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestWelshDictionary {

    @Test
    public void testRunWelshDictionary() {
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getWords("bod");
        Assertions.assertEquals(1, words.size());
    }

    @Test
    public void testHoffiWelshDictionary() {
        WelshDictionary dict = new WelshDictionary();
        List<Word> words = dict.getWords("cael");

        Assertions.assertEquals(1, words.size());
    }


    @Test
    public void testMawr(){

    }


}
